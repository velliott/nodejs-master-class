/*
 * Background log rotation worker related tasks
 *
 *
 */

// Dependencies
const _logs = require('../logs');
const util = require('util');
const debug = util.debuglog('workers');

// Rotate (compress) the logfiles
rotateLogs = function () {
    // List all the (non compressed) log files
    _logs.list(false, function (err, logs) {
        if (!err && logs && logs.length > 0) {
            logs.forEach(function (logName) {
                // Compress the data to a different file
                const logId = logName.replace('.log', '');
                const newFileId = logId + '-' + Date.now();
                _logs.compress(logId, newFileId, function (err) {
                    if (!err) {
                        // Truncate the log file
                        _logs.truncate(logId, function (err) {
                            if (!err) {
                                debug("Success truncating log file");
                            } else {
                                debug("Error truncating log file");
                            }
                        });
                    } else {
                        debug("Error compressing one of the log files", err);
                    }
                });
            });
        } else {
            debug("Could not find any log files to rotate");
        }
    });
};

// Export the module
module.exports = rotateLogs;