/*
 * Background worker related tasks
 *
 *
 */

// Dependencies
const gatherAllChecks = require("./gatherAllChecks");
const rotateLogs = require("./rotateLogs");

// Instantiate the worker object
const workers = {};

// Worker handlers

// Timer to execute the worker-process once per minute
loop = function() {
  setInterval(function() {
    gatherAllChecks();
  }, 1000 * 60);
};

// Timer to rotate log files once per day
logRotationLoop = function() {
  setInterval(function() {
    rotateLogs();
  }, 1000 * 60 * 60 * 24);
};

// Init script
workers.init = function() {
  // Log to console in YELLOW
  console.log("\x1b[33m%s\x1b[0m", "Background workers are running");

  // Execute all the checks immediately
  gatherAllChecks();

  // Call the loop so that the checks will execute later on
  loop();

  // Compress all the logs immediately
  rotateLogs();

  // Call the compression loop so logs will be compressed later on
  logRotationLoop();
};

// Export the module
module.exports = workers;
