/*
 * Background log to file worker related tasks
 *
 *
 */

// Dependencies
const _logs = require('../logs');
const util = require('util');
const debug = util.debuglog('workers');

// Log to files
logToFiles = function (originalCheckData, checkOutcome, state, alertWarranted, timeOfCheck) {
    // From the log data
    const logData = {
        'check': originalCheckData,
        'outcome': checkOutcome,
        'state': state,
        'alert': alertWarranted,
        'time': timeOfCheck
    };

    // Convert the data to a string
    const logString = JSON.stringify(logData);

    // Determine the name of the log file
    const logFileName = originalCheckData.id;

    // Append the log string to the file
    _logs.append(logFileName, logString, function (err) {
        if (!err) {
            debug("Logging to the file succeeded");
        } else {
            debug("Logging to the file failed");
        }
    });
};

// Export the module
module.exports = logToFiles;