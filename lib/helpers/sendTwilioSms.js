/*
 * Twilio Helper functions
 *
 */

// Dependencies
const config = require('../config');
const https = require('https');
const querystring = require('querystring');

// Send an SMS message via Twilio
sendTwilioSms = function (phone, msg, callback) {
    // Validate parameters
    phone = typeof (phone) == 'string' && phone.trim().length == 10 ? phone.trim() : false;
    msg = typeof (msg) == 'string' && msg.trim().length > 0 && msg.trim().length <= 1600 ? msg.trim() : false;
    if (phone && msg) {
        // Configure the request payload
        const payload = {
            'From': config.twilio.fromPhone,
            'To': '+1' + phone,
            'Body': msg
        };

        // Stringify the payload
        const stringPayload = querystring.stringify(payload);

        // Configure the request details
        const requestDetails = {
            'protocol': 'https:',
            'hostname': 'api.twilio.com',
            'method': 'POST',
            'path': '2010-04-01/Accounts/' + config.twilio.accountSid + '/Messages.json',
            'auth': config.twilio.accountSid + ':' + config.twilio.authToken,
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(stringPayload)
            }
        };

        // Instantiate the request object
        const req = https.request(requestDetails, function (res) {
            // Grab the status of the sent request
            const status = res.statusCode;
            // Callback successfully if the request went trhough
            if (status == 200 || status == 201) {
                callback(false);
            } else {
                // console.log(res);
                callback('Status code returned was ' + status);
            }
        });

        // Bind to the error event so it does not get thrown
        req.on('error', function (e) {
            callback(e);
        });

        // Add the payload
        req.write(stringPayload);

        // End the request
        req.end();

    } else {
        callback('Required parameters were missing or invalid');
    }
};

// Export the helpers
module.exports = sendTwilioSms;