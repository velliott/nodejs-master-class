/*
 * Library for storing and editing data
 *
 */

// Dependencies
const fs = require("fs");
const path = require("path");
const helpers = require("./helpers");

// Container for the module (to be exported)
const lib = {};

// Base directory of the data folder
lib.baseDir = path.join(__dirname, "/../.data/");

// Write data to a file
lib.create = function (dir, file, data, callback) {
  // Open the file for writing
  fs.open(lib.baseDir + dir + "/" + file + ".json", "wx", function (
    err,
    fileDescriptor
  ) {
    if (!err && fileDescriptor) {
      // Convert data to a string
      const stringData = JSON.stringify(data);

      // Write to file and close it
      fs.writeFile(fileDescriptor, stringData, function (err) {
        if (!err) {
          fs.close(fileDescriptor, function (err) {
            if (!err) {
              callback(false);
            } else {
              callback(
                `Error closing new file '${file}.json' in directory '${dir}'`
              );
            }
          });
        } else {
          callback(
            `Error writing to new file '${file}.json' in directory '${dir}'`
          );
        }
      });
    } else {
      callback(
        `Could not create a new file '${file}.json' in directory '${dir}', it may already exist`
      );
    }
  });
};

// Read data from a file
lib.read = function (dir, file, callback) {
  fs.readFile(lib.baseDir + dir + "/" + file + ".json", "utf8", function (
    err,
    data
  ) {
    if (!err && data) {
      const parsedData = helpers.parseJsonToObject(data);
      callback(false, parsedData);
    } else {
      callback(err, data);
    }
  });
};

// Update data inside a file
lib.update = function (dir, file, data, callback) {
  fs.open(lib.baseDir + dir + "/" + file + ".json", "r+", function (
    err,
    fileDescriptor
  ) {
    if (!err && fileDescriptor) {
      // Truncate the file
      fs.truncate(fileDescriptor, function (err) {
        if (!err) {
          // Convert data to a string
          const stringData = JSON.stringify(data);

          // Write to file and close it
          fs.writeFile(fileDescriptor, stringData, function (err) {
            if (!err) {
              fs.close(fileDescriptor, function (err) {
                if (!err) {
                  callback(false);
                } else {
                  callback(
                    `Error closing updated file '${file}.json' in directory '${dir}'`
                  );
                }
              });
            } else {
              callback(
                `Error writing update to existing file '${file}.json' in directory '${dir}'`
              );
            }
          });
        } else {
          callback(
            `Could not truncate existing file '${file}.json' in directory '${dir}'`
          );
        }
      });
    } else {
      callback(
        `Could not open '${file}.json' in directory '${dir}' for updating, it may not exist`
      );
    }
  });
};

// Delete a file
lib.delete = function (dir, file, callback) {
  // Unlink a file
  fs.unlink(lib.baseDir + dir + "/" + file + ".json", function (err) {
    if (!err) {
      callback(false);
    } else {
      callback(
        `Error deleting file '${file}.json' in directory '${dir}', it may not exist`
      );
    }
  });
};

// List all the items in a directory
lib.list = function (dir, callback) {
  fs.readdir(lib.baseDir + dir + "/", function (err, data) {
    if (!err && data && data.length > 0) {
      const trimmedFileNames = [];
      data.forEach(function (fileName) {
        if (fileName !== ".gitignore") trimmedFileNames.push(fileName.replace(".json", ""));
      });
      callback(false, trimmedFileNames);
    } else {
      callback(err, data);
    }
  });
};

// Export the module
module.exports = lib;
