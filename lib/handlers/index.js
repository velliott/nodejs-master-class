/*
 * Request handlers
 *
 */

// Dependencies
const users = require('./users');
const tokens = require('./tokens');
const checks = require('./checks');

// Define the handlers
const handlers = {};

// Define route handlers
handlers.users = users;
handlers.tokens = tokens;
handlers.checks = checks;

// Not found handler
handlers.notFound = function (data, callback) {
    callback(404);
}

// Helper handlers

// ping handler
handlers.ping = function (data, callback) {
    // Callback a http status code, and a payload queryStringObject
    callback(200);
};
;


// Export the module
module.exports = handlers;