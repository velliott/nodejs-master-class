/*
 * Check handlers
 *
 */

// Dependencies
const _data = require('../data');
const helpers = require('../helpers');
const config = require('../config');
const tokens = require('./tokens');

// Entry point for checks methods
const checks = function (data, callback) {
    const acceptableMethods = ['post', 'get', 'put', 'delete'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        checkHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

// Container for the checks method handlers
checkHandlers = {};

// Checks - post
// Required data: protocol, url, method, successCodes, timeoutSeconds
// Optional data: none
checkHandlers.post = function (data, callback) {
    // Check that all required data fields are filled out
    const protocol = typeof (data.payload.protocol) == 'string' && ['http', 'https'].indexOf(data.payload.protocol.trim()) > -1 ? data.payload.protocol.trim() : false;
    const url = typeof (data.payload.url) == 'string' && data.payload.url.trim().length > 0 ? data.payload.url.trim() : false;
    const method = typeof (data.payload.method) == 'string' && ['post', 'get', 'put', 'delete'].indexOf(data.payload.method.trim()) ? data.payload.method.trim() : false;
    const successCodes = typeof (data.payload.successCodes) == 'object' && data.payload.successCodes instanceof Array && data.payload.successCodes.length > 0 ? data.payload.successCodes : false;
    const timeoutSeconds = typeof (data.payload.timeoutSeconds) == 'number' && data.payload.timeoutSeconds % 1 === 0 && data.payload.timeoutSeconds >= 1 && data.payload.timeoutSeconds <= 5 ? data.payload.timeoutSeconds : false;

    if (protocol && url && method && successCodes && timeoutSeconds) {
        // Get the token from the headers
        const token = typeof (data.headers.token) == 'string' ? data.headers.token : false;
        // Lookup the user by reading the token
        _data.read('tokens', token, function (err, tokenData) {
            if (!err && tokenData) {
                const userPhone = tokenData.phone;

                // Lookup the user's data
                _data.read('users', userPhone, function (err, userData) {
                    if (!err && userData) {
                        const userChecks = typeof (userData.checks) == 'object' && userData.checks instanceof Array ? userData.checks : [];
                        // Verify that the user has less than the maximum number of checks per user
                        if (userChecks.length < config.maxChecks) {
                            // Create a random id for the check
                            const checkId = helpers.createRandomString(20);

                            // Create the check object, and include the user's phone as a xref key back to the user's record
                            const checkObject = {
                                'id': checkId,
                                'userPhone': userPhone,
                                'protocol': protocol,
                                'url': url,
                                'method': method,
                                'successCodes': successCodes,
                                'timeoutSeconds': timeoutSeconds
                            };

                            // Persist the check object
                            _data.create('checks', checkId, checkObject, function (err) {
                                if (!err) {
                                    // Add the new check id to the user's object
                                    userData.checks = userChecks;
                                    userData.checks.push(checkId);

                                    // Persist the updated user object
                                    _data.update('users', userPhone, userData, function (err) {
                                        if (!err) {
                                            callback(200, checkObject);
                                        } else {
                                            console.log(err);
                                            callback(500, { 'Error': 'Could not update the user with the new check' });
                                        }
                                    });
                                } else {
                                    console.log(err);
                                    callback(500, { 'Error': 'Could not create the new check' });
                                }
                            });

                        } else {
                            callback(400, { 'Error': 'The user already has the maximum number of checks (' + config.maxChecks + ')' });
                        }
                    } else {
                        callback(403);
                    }
                });
            } else {
                callback(403);
            }
        });
    } else {
        callback(400, { 'Error': 'Missing required inputs, or inputs are invalid' });
    }
};

// Checks - get
// Required data: id
// Optional data: none
checkHandlers.get = function (data, callback) {
    // Check that the id is valid
    const id = typeof (data.queryStringObject.id) == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false;
    if (id) {
        // Lookup the check
        _data.read('checks', id, function (err, checkData) {
            if (!err && checkData) {
                // Get the token from the headers
                const token = typeof (data.headers.token) == 'string' ? data.headers.token : false;
                // Verify that the given token is valid for the phone number
                tokens.verifyToken(token, checkData.userPhone, function (tokenIsValid) {
                    if (tokenIsValid) {
                        // Return the check data
                        callback(200, checkData);
                    } else {
                        callback(403);
                    }
                });
            } else {
                callback(404);
            }
        });
    } else {
        callback(400, { 'Error': 'Missing required field' });
    }
};

// Checks - put
// Required data: id
// Optional data: protocol, url, method, successCodes, timeoutSeconds (at least one must be specified)
checkHandlers.put = function (data, callback) {
    // Check for the required field
    const id = typeof (data.payload.id) == 'string' && data.payload.id.trim().length == 20 ? data.payload.id.trim() : false;

    // Check to make sure id is valid
    if (id) {
        // Check for the optional fields
        const protocol = typeof (data.payload.protocol) == 'string' && ['http', 'https'].indexOf(data.payload.protocol.trim()) > -1 ? data.payload.protocol.trim() : false;
        const url = typeof (data.payload.url) == 'string' && data.payload.url.trim().length > 0 ? data.payload.url.trim() : false;
        const method = typeof (data.payload.method) == 'string' && ['post', 'get', 'put', 'delete'].indexOf(data.payload.method.trim()) ? data.payload.method.trim() : false;
        const successCodes = typeof (data.payload.successCodes) == 'object' && data.payload.successCodes instanceof Array && data.payload.successCodes.length > 0 ? data.payload.successCodes : false;
        const timeoutSeconds = typeof (data.payload.timeoutSeconds) == 'number' && data.payload.timeoutSeconds % 1 === 0 && data.payload.timeoutSeconds >= 1 && data.payload.timeoutSeconds <= 5 ? data.payload.timeoutSeconds : false;

        if (protocol || url || method || successCodes || timeoutSeconds) {
            // Lookup the check
            _data.read('checks', id, function (err, checkData) {
                if (!err && checkData) {
                    // Get the token from the headers
                    const token = typeof (data.headers.token) == 'string' ? data.headers.token : false;
                    // Verify that the given token is valid for the phone number
                    tokens.verifyToken(token, checkData.userPhone, function (tokenIsValid) {
                        if (tokenIsValid) {
                            if (protocol) {
                                checkData.protocol = protocol;
                            }
                            if (url) {
                                checkData.url = url;
                            }
                            if (method) {
                                checkData.method = method;
                            }
                            if (successCodes) {
                                checkData.successCodes = successCodes;
                            }
                            if (timeoutSeconds) {
                                checkData.timeoutSeconds = timeoutSeconds;
                            }

                            // Persist the updates to the check
                            _data.update('checks', id, checkData, function (err) {
                                if (!err) {
                                    callback(200);
                                } else {
                                    console.log(err);
                                    callback(500, { 'Error': 'Could not update the check parameters' });
                                }
                            });
                        } else {
                            callback(403);
                        }
                    });
                } else {
                    callback(400, { 'Error': 'Check ID did not exist' });
                }
            });
        } else {
            callback(400, { 'Error': 'Missing fields to update' });
        }
    } else {
        callback(400, { 'Error': 'Missing required field' });
    }
};

// Checks - delete
// Required data: id
// Optional data: none
checkHandlers.delete = function (data, callback) {
    // Check that the id is valid
    const id = typeof (data.queryStringObject.id) == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false;
    if (id) {
        // Lookup the check
        _data.read('checks', id, function (err, checkData) {
            if (!err && checkData) {
                // Get the token from the headers
                const token = typeof (data.headers.token) == 'string' ? data.headers.token : false;
                // Verify that the given token is valid for the phone number
                tokens.verifyToken(token, checkData.userPhone, function (tokenIsValid) {
                    if (tokenIsValid) {
                        // Delete the check data
                        _data.delete('checks', id, function (err) {
                            if (!err) {
                                // Lookup the user
                                _data.read('users', checkData.userPhone, function (err, userData) {
                                    if (!err && userData) {
                                        const userChecks = typeof (userData.checks) == 'object' && userData.checks instanceof Array ? userData.checks : [];

                                        // Remove the check from their list of checks
                                        const checkPosition = userChecks.indexOf(id);
                                        if (checkPosition > -1) {
                                            console.log('userData before: ', userData);
                                            userChecks.splice(checkPosition, 1);
                                            console.log('userData after: ', userData);
                                            // userData.checks = userChecks;

                                            // Persist the updated user object
                                            _data.update('users', checkData.userPhone, userData, function (err) {
                                                if (!err) {
                                                    callback(200);
                                                } else {
                                                    console.log(err);
                                                    callback(500, { 'Error': 'Could not update the user' });
                                                }
                                            });
                                        } else {
                                            console.log(err);
                                            callback(500, { 'Error': 'Could not find the check amongst the user\'s list. Request ignored' })
                                        }
                                    } else {
                                        console.log(err);
                                        callback(500, { 'Error': 'Could not find the user who created the check.  Also, could not remove the check from the list of checks for the user.' });
                                    }
                                });
                            } else {
                                console.log(err);
                                callback(500, { 'Error': 'Could not delete the check data' });
                            }
                        });
                    } else {
                        callback(403);
                    }
                });
            } else {
                callback(400, { 'Error': 'The specified check ID does not exist' });
            }
        });
    } else {
        callback(400, { 'Error': 'Missing required field' });
    }
};

// Export the module
module.exports = checks;