/*
 * Token handlers
 *
 */

// Dependencies
const _data = require('../data');
const helpers = require('../helpers');
// const config = require('../config');

// Entry point for tokens methods
const tokens = function (data, callback) {
    const acceptableMethods = ['post', 'get', 'put', 'delete'];
    if (acceptableMethods.indexOf(data.method) > -1) {
        tokenHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

// Container for the tokens method handlers
tokenHandlers = {};

// Tokens - post handler
// Required data: phone, password
// Optional data: none
tokenHandlers.post = function (data, callback) {
    const phone = typeof (data.payload.phone) == 'string' && data.payload.phone.trim().length == 10 ? data.payload.phone.trim() : false;
    const email = typeof (data.payload.email) == 'string' && data.payload.email.trim().length > 0 ? data.payload.email.trim() : false;
    const password = typeof (data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
    if (phone && email && password) {
        // Lookup the user with the specified phone number
        _data.read('users', phone, function (err, userData) {
            if (!err) {
                // Hash the password sent and compare it to that stored for the user
                const hashedPassword = helpers.hash(password);
                if (hashedPassword == userData.hashedPassword) {
                    // If valid, create a new token with a random name.  Set expiration to be 1 hour ahead.
                    const tokenId = helpers.createRandomString(20);
                    const expires = Date.now() + (1000 * 60 * 60);
                    const tokenObject = {
                        'phone': phone,
                        'email': email,
                        'id': tokenId,
                        'expires': expires
                    };

                    // Store the token
                    _data.create('tokens', tokenId, tokenObject, function (err) {
                        if (!err) {
                            callback(200, tokenObject);
                        } else {
                            console.log(err);
                            callback(500, { 'Error': 'Could not create new token' });
                        }
                    });
                } else {
                    callback(400, { 'Error': 'Password did not match' });
                }
            } else {
                callback(400, { 'Error': 'Could not find the specified user' });
            }
        });
    } else {
        callback(400, { 'Error': 'Missing required field(s)' });
    }
};

// Tokens - get handler
// Required data: id
// Optional data: none
tokenHandlers.get = function (data, callback) {
    // Check that the token id is valid
    const id = typeof (data.queryStringObject.id) == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false;
    if (id) {
        // Lookup the token
        _data.read('tokens', id, function (err, tokenData) {
            if (!err && tokenData) {
                callback(200, tokenData);
            } else {
                callback(404);
            }
        });
    } else {
        callback(400, { 'Error': 'Missing required field' });
    }
};

// Tokens - put handler
// Required data: id, extend (boolean)
// Optional data: none
tokenHandlers.put = function (data, callback) {
    const id = typeof (data.payload.id) == 'string' && data.payload.id.trim().length == 20 ? data.payload.id.trim() : false;
    const extend = typeof (data.payload.extend) == 'boolean' && data.payload.extend == true ? true : false;
    if (id && extend) {
        // Lookup the token
        _data.read('tokens', id, function (err, tokenData) {
            if (!err && tokenData) {
                // Verify that the token has not expired
                if (tokenData.expires > Date.now()) {
                    // Set the expiration to be 1 hour from now
                    tokenData.expires = Date.now() + (1000 * 60 * 60);

                    // Store the updated token
                    _data.update('tokens', id, tokenData, function (err) {
                        if (!err) {
                            callback(200);
                        } else {
                            console.log(err);
                            callback(500, { 'Error': 'Could not update the token\'s expiration' });
                        }
                    });
                } else {
                    callback(400, { 'Error': 'The token has expired, and cannot be extended' });
                }
            } else {
                callback(400, { 'Error': 'Specified token does not exist' });
            }
        });
    } else {
        callback(400, { 'Error': 'Missing or invalid field(s)' });
    }
};

// Tokens - delete handler
// Required data: id
// Optional data: none
tokenHandlers.delete = function (data, callback) {
    // Check that the token id is valid
    const id = typeof (data.queryStringObject.id) == 'string' && data.queryStringObject.id.trim().length == 20 ? data.queryStringObject.id.trim() : false;
    if (id) {
        // Lookup the token
        _data.read('tokens', id, function (err, data) {
            if (!err && data) {
                // Delete the token
                _data.delete('tokens', id, function (err) {
                    if (!err) {
                        callback(200);
                    } else {
                        console.log(err);
                        callback(500, { 'Error': 'Could not delete the specified token' });
                    }
                });
            } else {
                callback(400, { 'Error': 'Could not find the record for the specified token' });
            }
        });
    } else {
        callback(400, { 'Error': 'Missing required field' });
    }
};

// Verify if a given token id is currently valid for a given user
tokens.verifyToken = function (id, phone, callback) {
    // Lookup the token
    _data.read('tokens', id, function (err, tokenData) {
        if (!err && tokenData) {
            // Check that the token is for the given user and has not expired
            if (tokenData.phone == phone && tokenData.expires > Date.now()) {
                callback(true);
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    });
};

// Export the module
module.exports = tokens;