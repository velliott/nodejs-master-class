/*
 * Server related tasks
 *
 */

// Dependencies
const http = require('http');
const https = require('https');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
const config = require('./config');
const fs = require('fs');
const handlers = require('./handlers');
const helpers = require('./helpers');
const path = require('path');
const util = require('util');
const debug = util.debuglog('server');

// @TODO GET RID OF THIS
// helpers.sendTwilioSms('4158375309', 'Hello from Twilio TEST!', function (err) {
//     debug('This was the error: ', err);
// });

// Instantiate the server object
const server = {};

// Instantiate the HTTP server
server.httpServer = http.createServer(function (req, res) {
    server.unifiedServer(req, res);
});

// Instantiate the HTTPS server
server.httpsServerOptions = {
    'key': fs.readFileSync(path.join(__dirname, '/../https/key.pem')),
    'cert': fs.readFileSync(path.join(__dirname, '/../https/cert.pem'))
};
server.httpsServer = https.createServer(server.httpsServerOptions, function (req, res) {
    server.unifiedServer(req, res);
});

// All the server logic for both the http and https server(s)
server.unifiedServer = function (req, res) {

    // Get the URL and parse it
    //debug('req: ',req);
    const parsedUrl = url.parse(req.url, true);
    debug('parsed url: ', parsedUrl);

    // Get the path
    const path = parsedUrl.pathname;
    //debug('path: ',path);
    const trimmedPath = path.replace(/^\/+|\/+$/g, '');
    //debug('trimmedPath: ',trimmedPath);

    // Get the query stringas an object
    const queryStringObject = parsedUrl.query;

    // Get the HTTP method
    const method = req.method.toLowerCase();

    // Get the request headers as an queryStringObject
    const headers = req.headers;

    // Get the payload, if any
    const decoder = new StringDecoder('utf-8');
    var buffer = '';
    req.on('data', function (data) {
        buffer += decoder.write(data);
    });
    req.on('end', function () {
        buffer += decoder.end();

        // Choose the handler this request should go to. If  one is not found, use the notFound handler.
        const chosenHandler = typeof (server.router[trimmedPath]) !== 'undefined' ? server.router[trimmedPath] : handlers.notFound;

        // Construct the data object to send to the handler
        const data = {
            'trimmedPath': trimmedPath,
            'queryStringObject': queryStringObject,
            'method': method,
            'headers': headers,
            'payload': helpers.parseJsonToObject(buffer)
        };
        debug('data', data);

        // Route the request to the handler specified in the router
        chosenHandler(data, function (statusCode, payload) {
            // Use the status code returned by the handler, or default to 200
            statusCode = typeof (statusCode) == 'number' ? statusCode : 200;

            // Use the payload returned by the handler, or default to an empty object
            payload = typeof (payload) == 'object' ? payload : {};

            // Convert the payload to a string
            const payloadString = JSON.stringify(payload);

            // Return the response
            res.setHeader('Content-Type', 'application/json');
            res.writeHead(statusCode);
            res.end(payloadString);

            // Log the request path
            if (statusCode == 200) {
                debug('\x1b[32m%s\x1b[0m', method.toUpperCase() + ' /' + trimmedPath + ' ' + statusCode);
            } else {
                debug('\x1b[31m%s\x1b[0m', method.toUpperCase() + ' /' + trimmedPath + ' ' + statusCode);
                // debug('\x1b[31m%s\x1b[0m', 'Request path: ' + trimmedPath + ', method: ' + method + ', query parameters: ', queryStringObject);
                // debug('\x1b[31m%s\x1b[0m', 'Request headers: ', headers);
                // debug('\x1b[31m%s\x1b[0m', 'Request payload: ', buffer);
            }
        });
    });

};

// Define a request router
server.router = {
    'ping': handlers.ping,
    'users': handlers.users,
    'tokens': handlers.tokens,
    'checks': handlers.checks
};

// Init script
server.init = function () {
    // Start the HTTP server
    server.httpServer.listen(config.httpPort, function () {
        console.log('\x1b[36m%s\x1b[0m', `The HTTP server is listening on port ${config.httpPort} in ${config.envName} now!`);
    });

    // Start the HTTPS server
    server.httpsServer.listen(config.httpsPort, function () {
        console.log('\x1b[35m%s\x1b[0m', `The HTTPS server is listening on port ${config.httpsPort} in ${config.envName} now!`);
    });

};

module.exports = server;